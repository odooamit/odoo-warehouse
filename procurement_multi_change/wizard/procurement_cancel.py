# coding: utf-8
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 06.12.2016.
#
#    This module is an extension of procurement_cancel. Thanks go to Vauxoo SA.

import logging

from openerp.osv import osv

_logger = logging.getLogger(__name__)


class CancelProcurementOrder(osv.osv_memory):
    _name = 'cancel.procurement.order'
    _description = 'Cancel procurement'

    def confirm_multi_procurement(self, cr, uid, ids, context=None):
        pr_obj = self.pool['procurement.order']
        _logger.info('Confirm cancelled  Procurements')
        pr_obj.reset_to_confirmed(cr, uid, context.get('active_ids', False), context=None)
        return {'type': 'ir.actions.act_window_close'}

    def run_multi_procurement(self, cr, uid, ids, context=None):
        pr_obj = self.pool['procurement.order']
        _logger.info('Run confirmed  Procurements')
        pr_obj.run(cr, uid, context.get('active_ids', False), context=None)
        return {'type': 'ir.actions.act_window_close'}

    def cancel_procurement(self, cr, uid, ids, context=None):
        """@param self: The object pointer.
        @param cr: A database cursor
        @param uid: ID of the user currently logged in
        @param ids: List of IDs selected
        @param context: A standard dictionary
        """

        pr_obj = self.pool['procurement.order']
        _logger.info('Cancel Procurement is running')
        pr_obj.cancel(cr, uid, context.get('active_ids', False), context=None)
        return {'type': 'ir.actions.act_window_close'}
