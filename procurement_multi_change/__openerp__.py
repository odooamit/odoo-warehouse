# coding: utf-8
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 06.12.2016.
#
#    This module is an extension of procurement_cancel. Thanks go to Vauxoo SA.

{
    "name": "Procurement Multi Change",
    "version": "8.0.1.0.0",
    "category": "Tools",
    "author": "Jamotion GmbH",
    "website": "http://www.vauxoo.com/",
    "summary": "Allow multi cancel, confirm and run of Procurements",
    "license": "AGPL-3",

    "depends": [
        "base",
        "procurement",
    ],
    "demo": [],
    "data": [
        "security/procurement_cancel.xml",
        "wizard/procurement_cancel_view.xml",
    ],
    "installable": True,
}
